CC=gcc

all: apl1 apl2 apl3

apl1:
	$(CC) -c A1/*.c 
	mv -f *.o A1/obj/
	$(CC) A1/obj/*.o -o sudoku-P1
apl2:
	$(CC) -c A2/*.c 
	mv -f *.o A2/obj/
	$(CC)  A2/obj/*.o -o sudoku-P2 -lcurses

apl3:
	$(CC) -c A3/*.c 
	mv -f *.o A3/obj/
	$(CC) A3/obj/*.o -o sudoku-P3 -lcurses


clean:
	rm -f */obj/*.o

realclean: 
	rm -f  *~ sudoku.*