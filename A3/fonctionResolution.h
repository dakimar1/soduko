#ifndef H_fonctionResolution

#define H_fonctionResolution

void initialiseposibilite(Case grille[9][9]);
void applicationR2(Case grille[9][9]);
void applicationR3(Case grille[9][9]);
void applicationR4(Case grille[9][9]);
bool recherchExaustive(Case grille[9][9], int position);

#endif
