#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <ncurses.h>
#include "menuJeu.h"
#include "selectionNiveaux.h"


int n_choices;

void print_menu_Niveaux(WINDOW *menu_win, int highlight,char* choices[])
{
    int x, y, i;
    x = 2;
    y = 2;
    box(menu_win, 0, 0);
    for(i = 0; i < n_choices; ++i)
    {
        if(highlight == i + 1) 
        {
            wattron(menu_win,A_REVERSE | A_BLINK);
            mvwprintw(menu_win, y, x, "%s", choices[i]);
            wattroff(menu_win,A_REVERSE | A_BLINK );
        }
        else
            mvwprintw(menu_win, y, x, "%s", choices[i]);
        ++y;
    }
    wrefresh(menu_win);
}

void NavigueDansMenuNiveaux(WINDOW *niveaux_win, int highlight,char* choices[])
{
    int c;
    mvprintw(24,0,"Utilise soit la touche haut ou bas pour choisir puis entrer");
    int choice=0;
    while(1)
    {
        c = wgetch(niveaux_win);
        switch(c)
        {
        case KEY_UP:
            if(highlight == 1)
                highlight =  n_choices;
            else
                --highlight;
            break;
        case KEY_DOWN:
            if(highlight ==  n_choices)
                highlight = 1;
            else
                ++highlight;
            break;
        case 10:
            choice = highlight;
            break;
        default:
            break;
        }
        print_menu_Niveaux(niveaux_win, highlight,choices);
        if(choice != 0)  	
        {
            char* fichier;
            fichier=(char*)malloc(sizeof(char)*20);
            if (choice==1) fichier="sudoku1.txt" ;
            else if (choice==2) fichier="sudoku2.txt";
            else if (choice==3) fichier="sudoku3.txt";
            else if (choice==4)
            {
                clear();
                echo();
                mvprintw(0,0,"Entrez votre propre sudoku : ");
                getstr(fichier);
            }
            if (choice==5) return ;
            menujeu(fichier);
            return ;
        };
    }
}

int selectionNiveau()
{
    char *choices[] =
{
    "Facile",
    "Moyen",
    "Dificile",
    "Autre",
    "Exit",
};
    n_choices = sizeof(choices) / sizeof(char *);
    WINDOW *niveaux_win;
    int highlight = 1;
    clear();
    noecho();
    cbreak();
    niveaux_win = newwin(10,22, LINES/2-5, COLS/2-11);
    keypad(niveaux_win, TRUE);
    attron(A_BOLD);
    mvprintw(0,0,"Selectioner le niveaux de dificulte");
    attron(A_BOLD);
    refresh();
    init_pair(3, COLOR_BLUE,COLOR_WHITE );
    wbkgd(niveaux_win,COLOR_PAIR(3));
    print_menu_Niveaux(niveaux_win, highlight,choices);
    NavigueDansMenuNiveaux(niveaux_win,highlight,choices);
    clrtoeol();
    refresh();
    endwin();
    return 0;
}


