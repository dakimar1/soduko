#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "typeCase.h"
#include "fonctionGrille.h"
#include "fonctionResolution.h"





void initialiseposibilite(Case grille[9][9])
{
    int i,j,k;
    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            grille[i][j].nbrDePos=0;
        }
    }

    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            for(k=1; k<=9; k++)
            {
                if (absentSurLigne(k,grille,i) && absentSurColonne(k,grille,j) && absentSurBloc(k,grille,i, j))
                {
                    grille[i][j]=ajoutElemntCase(grille[i][j],k);
                }
            }
        }
    }
}

void applicationR2(Case grille[9][9])
{
    int i,j;
    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            if (grille[i][j].nbrDePos==1)
            {
                grille[i][j]=validationDeCase(grille[i][j],grille[i][j].valPossib[0]);
                miseJourGrille(grille,i,j,grille[i][j].Valvalide);
                applicationR2(grille);
            }
        }
    }
}

void applicationR3(Case grille[9][9])
{
    int i,j,k;
    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            for(k=0; k<grille[i][j].nbrDePos; k++)
            {
                if (absentR3(k,grille,i,j))
                {
                    grille[i][j]=validationDeCase(grille[i][j],grille[i][j].valPossib[k]);
                    miseJourGrille(grille,i,j,grille[i][j].Valvalide);
                    applicationR3(grille);
                }
            }
        }
    }

}

void applicationR4(Case grille[9][9])
{
    int i,j,k;
    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            for(k=0; k<grille[i][j].nbrDePos; k++)
            {
                if (absentSurLigne_R4(grille,i,j,grille[i][j].valPossib[k])&&
                        presnet_R4(grille,i,j,grille[i][j].valPossib[k])
                        && absent_R4(grille,i,j,grille[i][j].valPossib[k])
                   )
                {

                    supprimer_R4(grille,i,j,grille[i][j].valPossib[k]);

                }
            }
        }
    }
}

bool recherchExaustive(Case grille[9][9], int position)
{
    if (position == 9*9)
        return true;

    int i = position/9, j = position%9;
    int k;

    if (grille[i][j].valide)
        return recherchExaustive(grille, position+1);

    for (k=0; k <9; k++)
    {
        if (absentSurLigne(k,grille,i) && absentSurColonne(k,grille,j) && absentSurBloc(k,grille,i,j))
        {
            grille[i][j] = validationDeCase(grille[i][j],k);

            if ( recherchExaustive (grille, position+1) )
                return true;
        }
    }
    grille[i][j]=deValidationDeCase(grille,i,j);
    return false;
}


