/*
 *	Name: Resolution d'un sudoku par aplication des regles avec ncurses
 *
 *	Desc:
 * le joueur resoud tous seul la grille .
 * il peux revenir ou aller a la grille suivant ou precedent.
 * il peux demander de l'aide.
 *
 *	Author: Daki Marouane	(daki.marouane@gmail.com).
 *			Douelfakar Omar  (douelfakar.omar@gmail.com).
 *
 *	Date: 25.Avr.2012
 *
 */

#include <stdio.h>
#include <string.h>
#include <ncurses.h>
#include "selectionNiveaux.h"

int n_choices;

void print_menu(WINDOW *menu_win, int highlight,char* choices[])
{
    int x, y, i;
    x = 2;
    y = 2;
    box(menu_win, 0, 0);
    for(i = 0; i < n_choices; ++i)
    {
        if(highlight == i + 1) 
        {
            wattron(menu_win,A_REVERSE | A_BLINK);
            mvwprintw(menu_win, y, x, "%s", choices[i]);
            wattroff(menu_win,A_REVERSE | A_BLINK );
        }
        else
            mvwprintw(menu_win, y, x, "%s", choices[i]);
        ++y;
    }
    wrefresh(menu_win);
}

void navigueDansMenu(WINDOW *menu_win, int highlight,char* choices[])
{
    int c;
    mvprintw(24, 0, "Utilise soit la touche haut ou bas pour choisir, puis entrer pour faire votre choix ");
    int choice=0;
    while(1)
    {
        c = wgetch(menu_win);
        switch(c)
        {
        case KEY_UP:
            if(highlight == 1)
                highlight = n_choices;
            else
                --highlight;
            break;
        case KEY_DOWN:
            if(highlight == n_choices)
                highlight = 1;
            else
                ++highlight;
            break;
        case 10:
            choice = highlight;
            break;
        default:
            break;
        }
        print_menu(menu_win, highlight,choices);
        if(choice == 1)  	
        {
            selectionNiveau();
            return ;
        }
        else if (choice == 2) return ;
    }
}

int main()
{
    char *choices[] =
    {
        "Nouvelle Partie",
        "Quitter",
    };
    n_choices = sizeof(choices) / sizeof(char *);
    WINDOW *menu_win;
    int highlight = 1;
    initscr();
    clear();
    noecho();
    cbreak();	
    int startx1 =  COLS/2 - 10;
    int starty1 =  LINES/2 - 4;
    menu_win = newwin(7,20, starty1, startx1);
    keypad(menu_win,TRUE);
    start_color();
    init_pair(2, COLOR_WHITE, COLOR_BLUE);
    init_pair(3, COLOR_BLUE,COLOR_WHITE );
    bkgd(' ' | COLOR_PAIR(2));
    attron(A_BOLD);
    mvprintw(0, 0, "Utilisez les touches fl�ch�es pour monter et descendre, Appuyez sur Entr�e pour s�lectionner un choix");
    attron(A_BOLD);
    refresh();
    wbkgd(menu_win,COLOR_PAIR(3));
    print_menu(menu_win, highlight,choices);
    navigueDansMenu(menu_win,highlight,choices);
    clrtoeol();
    refresh();
    endwin();
    return 0;
}


