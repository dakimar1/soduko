#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ncurses.h>
#include <time.h>

#include "typeCase.h"
#include "fonctionGrille.h"
#include "menuJeu.h"
#include "Sauvgardage.h"
#include "resolution.h"

#define WIDTH 14
#define HEIGHT 28

Case grilleSol[9][9];

void affichagerGille1(WINDOW *grille_win,Case grille[9][9],int ii,int jj)
{
    int i,j,x,y;
    x = 2;
    y = 1;
    box(grille_win, 0, 0);
    for(i=0; i<9; i++)
    {


        for(j=0,x=2; j<9; j++,x=x+2)
        {
            if(i==ii && j==jj)
            {
                wattron(grille_win,A_REVERSE | A_BLINK);
                mvwprintw(grille_win, y, x, "%d", grille[i][j]);
                wattroff(grille_win,A_REVERSE | A_BLINK );
            }
            else
            {
                mvwprintw(grille_win, y, x, "%d", grille[i][j]);
            }

            if ((j+1)%3==0 && j!=8)
            {
                x=x+2;
                mvwprintw(grille_win, y, x, "||");
                x++;
            }
        }
        y=y+1;
        if ((i+1)%3==0)
        {
            mvwprintw(grille_win, y, 2,"------------------------");
            y=y+1;
        }
    }
    wrefresh(grille_win);
    mvprintw(LINES-4, 0,"Echap  : pour Quitter");
    mvprintw(LINES-3, 0,"h      : pour l'Aide");
    mvprintw(LINES-2, 0,"n      : pour Next");
    mvprintw(LINES-1, 0,"b      : pour Back");
    refresh();
}

int verfifictaion(Case grille[9][9],Case grilleSol[9][9])
{
    int i,j;
    for (i=0; i<9; i++)
    {
        for (j=0; j<9; j++)
        {
            if (!(grille[i][j].valide)) return 0;
        }
    }
    for (i=0; i<9; i++)
    {
        for (j=0; j<9; j++)
        {
            if (grille[i][j].Valvalide!=grilleSol[i][j].Valvalide) return -1;
        }
    }
    return 1;
}

void navigueDansMenuJeu(WINDOW *menu_win,Case grille[9][9],TAB_Grille TabGrille)
{
    int c,i=0,j=0;
    int choice=0;
    time_t debut = time(NULL);
    time_t fin=0;
    while(1)
    {
        if (verfifictaion(grille,grilleSol)==-1)
        {
            clear();
            mvprintw(0,0,"le sudoku n'a pas etait bien resolue");
        }
        else if(verfifictaion(grille,grilleSol)==1)
        {
            clear();
            if (fin==0) fin = time(NULL);
            mvprintw(0,0,"felicitation vous avez resloue le sudoku");
            mvprintw(1,0,"dur� : %dm %ds",(fin-debut)/60,(fin-debut)%60);
        }
        else
        {
            clear();
        }
        c = wgetch(menu_win);
        switch(c)
        {
        case KEY_UP:
            if(i == 0)
                i = 8;
            else
                --i;
            break;
        case KEY_DOWN:
            if(i == 8)
                i = 0;
            else
                ++i;
            break;
        case KEY_RIGHT:
            if (j==8 && i==8)
            {
                j=0;
                i=0;
            }
            else if(j == 8)
            {
                j = 0;
                i++;
            }
            else
                j++;
            break;
        case KEY_LEFT :
            if (j==0 && i==0)
            {
                j=8;
                i=8;
            }
            else if(j == 0)
            {
                j = 8;
                i--;
            }
            else
                j--;
            break;
        case 10:
            choice = 0;
            break;
        case 98:
            TabGrille=go_prec(TabGrille);
            copiGrille(TabGrille->grille,grille);
            break;
        case 110:
            TabGrille=go_Suiv(TabGrille);
            copiGrille(TabGrille->grille,grille);
            break;
        case 104:
            TabGrille=ajoute_sauvegarde(TabGrille,grille);
            fonctionHelp(grilleSol,grille);
            break;
        case 27:
            return ;
            break;
        default:
            if (49<=c && 57>=c)
            {
                c=c-48;
                if (!(grille[i][j].valide))
                {
                    grille[i][j]=validationDeCase(grille[i][j],c);
                    TabGrille=ajoute_sauvegarde(TabGrille,grille);
                }
                else
                {
                    clear();
                    mvprintw(0, 0, "cette case est deja valide");
                }
            }

            break;
        }
        refresh();
        affichagerGille1(menu_win,grille,i,j);
    }
}






void menujeu(char fichier[20])
{
    if (fopen(fichier,"r")==NULL)
    {
        perror("ERROR Fichier\n");
        printf("appyuer sur une touche pour quitter\n");
        getchar();
        return;
    }
    WINDOW *menu_win;
    srand(time(NULL));
    TAB_Grille tabGrille;
    tabGrille=initialise_sauvegarde(fichier);
    lectureGrille(fichier,grilleSol);
    resolutionGrille(grilleSol);
    initscr();
    clear();
    noecho();
    cbreak();
    menu_win = newwin(WIDTH, HEIGHT, 2, 15);
    box(menu_win,ACS_VLINE,ACS_HLINE);
    keypad(menu_win, TRUE);
    wbkgd(menu_win,COLOR_PAIR(3));
    refresh();
    affichagerGille1(menu_win, tabGrille->grille,0,0);
    navigueDansMenuJeu(menu_win,tabGrille->grille,tabGrille);
    clrtoeol();
    refresh();
    endwin();
}


