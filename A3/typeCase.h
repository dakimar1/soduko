#ifndef H_typeCase

#define H_typeCase


typedef struct T_Case
{
    int Valvalide;
    int *valPossib;
    int nbrDePos;
    bool valide;
} Case;

Case creerCase(int val);
bool valPossible(Case x,int val);
Case ajoutElemntCase(Case x,int val);
Case suprimElementCase(Case x,int val);
Case validationDeCase(Case x,int k);
bool absentR3(int k,Case grille[9][9], int i, int j);
Case deValidationDeCase(Case grille);

#endif
