#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "typeCase.h"

/*
Description des donnees : val un entier
creer un case avec la valeur val
Description des resultats : une case avec la valeur val
*/
Case creerCase(int val)
{
    if (val==0)
    {
        Case x;
        x.Valvalide=0;
        x.nbrDePos=0;
        x.valPossib=malloc(sizeof(int)*0);
        x.valide=false;
        return x;
    }
    else
    {
        Case x;
        x.Valvalide=val;
        x.nbrDePos=0;
        x.valide=true;
        return x;
    }
}

/*
Description des donnees : une Case x et entier val
Determine si la valeur val est une valeur possible pour la Case x
Description des resultats : un Bool
*/
bool valPossible(Case x,int val)
{
    int i;
    if (x.valide==false )
    {
        for(i=0; i<x.nbrDePos; i++)
        {
            if (x.valPossib[i]==val) return false;
        }
        return true;
    }
    else
    {
        return false;
    }
}

/*
Description des donnees : une Case x et entier val
ajoute la valeur val au possibilite de la Case x
Description des resultats :une Case
*/
Case ajoutElemntCase(Case x,int val)
{
    if (x.valide==false && valPossible(x,val)==true )
    {
        x.nbrDePos=x.nbrDePos+1;
        x.valPossib=realloc(x.valPossib,x.nbrDePos * sizeof(int) );
        x.valPossib[x.nbrDePos-1]=val;
        return x;
    }
    else
    {
        return x;
    }
}

/*
Description des donnees : une Case x et entier val
supprime la valeur val au possibilite de la Case x
Description des resultats : une Case
*/
Case suprimElementCase(Case x,int val)
{
    if (x.valide==false)
    {
        int i=0,j;
        while(i!=x.nbrDePos)
        {
            if(x.valPossib[i]==val)
            {
                for(j=i; j<(x.nbrDePos-1); j++)
                {
                    x.valPossib[j]=x.valPossib[j+1];


                }
                x.nbrDePos=x.nbrDePos-1;
                break;
            }
            else
            {
                i++;
            }
        }
        return x;
    }
    else
    {
        return x;
    }
    return x;
}

/*
Description des donnees : une Case x et entier val
valide la Case x avec la valeur val
Description des resultats : une Case
*/
Case validationDeCase(Case x,int k)
{
    x.valide=true ;
    x.Valvalide=k;
    x.nbrDePos=0;
    free(x.valPossib);
    x.valPossib=NULL;
    return x;
}

/*
Description des donnees : une Case x et entier val
devalide la Case x
Description des resultats : une Case
*/
Case deValidationDeCase(Case grille)
{
    grille.valide=false ;
    grille.Valvalide=0;
    return grille;
}












