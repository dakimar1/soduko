/*
 *	Name: Resolution d'un sudoku
 *
 *	Desc:
 * Implementation des 4 regles R1, R2, R3 et R4 .
 * Les grilles sont lues a partir de fichiers textes, la solution est placee dans un fichier texte.
 * L'afichage est en mode texte.
 * Recherche exhaustive (ou force brute) : on essaie de maniniere systematique toutes les
 * grilles encore candidates jusqu'a trouver la bonne.
 *
 *	Author: Daki Marouane	(daki.marouane@gmail.com).
 *			Doualfakar Omar
 *
 *	Date: 25.Avr.2012
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "typeCase.h"
#include "fonctionGrille.h"
#include "resolution.h"

int main(int argc,char* argv[])
{
    if (argc==0)//on teste si le nombre d'argument est bon
    {
        perror("Usage : fichierSource [fichierdestination]");
        exit(1);
    }
    if (fopen(argv[1],"r")==NULL)//on teste si le fichier existe
    {
        perror("ERROR Fichier\n");
        exit(2);
    }
    Case grille[9][9];
    lectureGrille(argv[1],grille);
	printf("Voici le sudoku avant resolution :\n\n");
    affichageGrille(grille);
    printf("\n");
    resolutionGrille(grille);
	printf("Voici le sudoku apres resolution :\n\n");
    affichageGrille(grille);
    //on regarde ou on vas ecrire le fichier par rapport
	//au nombre d'argument
	if (argc==3)
    {
        ecriturGrille(argv[2],grille);
    }
    else
    {
        char fdest[20]="solution-";
        strcat(fdest,argv[1]);
        ecriturGrille(fdest,grille);
    }
    return 0;
}

