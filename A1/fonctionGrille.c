#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "typeCase.h"
#include "fonctionGrille.h"


/*
Description des donnees : une grille
affiche la grille en question
Description des resultats : rien
*/
void affichageGrille(Case grille[9][9])
{
    int i,j;
    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            printf("%d ", grille[i][j].Valvalide);
            if ((j+1)%3==0 && j!=8) printf("|| ");
        }
        printf("\n");
        if ((i+1)%3==0) printf("------------------------\n");
    }
}


/*
Description des donnees : une chaine de caractere fSource ,une grille
lit une grille a partir du fichier fSource et la place la grille
Description des resultats : rien
*/
void lectureGrille(char* fSource,Case grille[9][9])
{
    int i,j,elem;
    FILE* fichier;
    fichier = fopen(fSource,"r");
    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            fscanf(fichier,"%d", &elem);
            grille[i][j]=creerCase(elem);
        }
    }
    fclose(fichier);
}


/*
Description des donnees : une chaine de caractere fSource ,une grille
ecrie la grille pass� en parametre dans le fichier fSource
Description des resultats : rien
*/
void ecriturGrille(char* fSource,Case grille[9][9])
{
    int i,j;
    FILE* fichier;
    fichier = fopen(fSource,"w+");
    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            fprintf(fichier,"%d ", grille[i][j].Valvalide);
        }
        fprintf(fichier,"\n");
    }
    fclose(fichier);
}


/*
Description des donnees : une grille
Un candidat a une case ne peut etre qu'un chifre non encore place dans un groupe (ligne, colonne et
region) qui contient cette case
Description des resultats :
*/
void initialiseposibilite(Case grille[9][9])
{
    int i,j,k;
    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            grille[i][j].nbrDePos=0;
        }
    }

    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            for(k=1; k<=9; k++)
            {
                if (absentSurLigne(k,grille,i) && absentSurColonne(k,grille,j) && absentSurRegion(k,grille,i, j))
                {
                    grille[i][j]=ajoutElemntCase(grille[i][j],k);
                }
            }
        }
    }
}

/*
Description des donnees : entier val,une grille, la ligne i entier
regarde si val n'est pas dans la ligne i de la grille
Description des resultats : vrai si absent sinon faux
*/
bool absentSurLigne(int val,Case grille[9][9], int i)
{
    int j;
    for (j=0; j < 9; j++)
        if (grille[i][j].Valvalide == val)
            return false;
    return true;
}


/*
Description des donnees : entier val,une grille, la colone j entier
regarde si val n'est pas dans la colone j de la grille
Description des resultats : vrai si absent sinon faux
*/
bool absentSurColonne(int val, Case grille[9][9], int j)
{
    int i;
    for (i=0; i < 9; i++)
        if (grille[i][j].Valvalide == val)
            return false;
    return true;
}

/*
Description des donnees : entier val,une grille, la ligne i entier, la colone j entier,
regarde si val n'est pas dans la region qui corespond a la ligne i et la colone j de la grille
Description des resultats : vrai si absent sinon faux
*/
bool absentSurRegion(int val, Case grille[9][9], int i, int j)
{
    int _i = i-(i%3), _j = j-(j%3);  // ou encore : _i = 3*(i/3), _j = 3*(j/3);
    for (i=_i; i < _i+3; i++)
        for (j=_j; j < _j+3; j++)
            if (grille[i][j].Valvalide == val)
                return false;
    return true;
}

/*
Description des donnees : entier val,une grille, la ligne i entier, la colone j entier,
regarde si val n'est pas une possibilite pour toutes les case de la ligne i,
sauf celle qui se trouve a j
Description des resultats : vrai si absent sinon faux
*/
bool absentSurLigne_R3(int val,Case grille[9][9], int i, int j)
{
    int p,jj;
    for (jj=0; jj < 9; jj++)
    {
        for(p=0; p<grille[i][jj].nbrDePos; p++)
        {
            if (grille[i][jj].valPossib[p]==val && jj!=j)
            {
                return false;
            }
        }
    }
    return true;
}

/*
Description des donnees : entier val,une grille, la ligne i entier, la colone j entier,
regarde si val n'est pas une possibilite pour toutes les case de la colone j,
sauf celle qui se trouve a i
Description des resultats : vrai si absent sinon faux
*/
bool absentSurColonne_R3(int val,Case grille[9][9], int i, int j)
{
    int p,ii;
    for (ii=0;ii<9;ii++)
    {
        for(p=0; p<grille[ii][j].nbrDePos; p++)
        {
            if (grille[ii][j].valPossib[p]==val && ii!=i) return false;
        }
    }
    return true ;
}
/*
Description des donnees : entier val,une grille, la ligne i entier, la colone j entier,
regarde si val n'est pas une possibilite pour toutes les case de la Region i,j ,
sauf celle qui se trouve a i,j
Description des resultats : vrai si absent sinon faux
*/
bool absentSurRegion_R3(int val,Case grille[9][9], int i, int j)
{
    int p,ii,jj;
    for (ii=(i-(i%3)); ii<(i-(i%3)+3); ii++)
    {
        for (jj=(j-(j%3)); jj < (j-(j%3)+3); jj++)
        {
            for(p=0; p<grille[ii][jj].nbrDePos; p++)
            {
                if ((grille[ii][jj].valPossib[p]==val && i!=ii) || (grille[ii][jj].valPossib[p]==val && jj!=j)) return false;
            }
        }
    }
    return true;
}

/*
Description des donnees : entier val,une grille, la ligne i entier, la colone j entier,
on regarde si val n'est pas present dans la Region i,j sauf dans la ligne i
Description des resultats : vrai si absent sinon faux
*/
bool absent_R4(Case grille[9][9],int i,int j,int val)
{
    int p,ii,jj;
    for (ii=(i-(i%3));ii<(i-(i%3)+3);ii++)
    {
        for (jj=(j-(j%3)); jj < (j-(j%3)+3);jj++)
        {
            for(p=0; p<grille[ii][jj].nbrDePos; p++)
            {
                if (grille[ii][jj].valPossib[p] == val && i!=ii)  return false;
            }
        }
    }
    return true;
}

/*
Description des donnees : entier val,une grille, la ligne i entier, la colone j entier,
on regarde si val n'est pas present dans la ligne i.
on regarde pas dans les case qui sont dans la Region i,j
Description des resultats : vrai si absent sinon faux
*/
bool absentSurLigne_R4(Case grille[9][9],int i,int j,int val)
{
    int p,k1,k2,jj;
    if(j%3==0)
    {
        k1=1;
        k2=2;
    }
    else if(j%3==1)
    {
        k1=-1;
        k2=1;
    }
    else
    {
        k1=-1;
        k2=-2;
    }
    for (jj=0; jj < 9; jj++)
    {
        for(p=0; p<grille[i][jj].nbrDePos; p++)
        {
            if (grille[i][jj].valPossib[p]==val && jj!=j && jj!=(j+k2) && j!=(j+k1))
            {
                return true;
            }
        }
    }
    return false;
}

/*
Description des donnees : entier val,une grille, la ligne i entier, la colone j entier,
on supprime val si il est present dans la ligne i,
on ne supprime pas dans les case qui sont dans la Region i,j.
Description des resultats : vrai si absent sinon faux
*/
void supprimer_R4(Case grille[9][9],int i,int j,int val)
{
    int p,k1,k2,jj;
    if(j%3==0)
    {
        k1=1;
        k2=2;
    }
    else if(j%3==1)
    {
        k1=-1;
        k2=1;
    }
    else
    {
        k1=-1;
        k2=-2;
    }
    for (jj=0; jj < 9; jj++)
    {
        for(p=0; p<grille[i][jj].nbrDePos; p++)
        {
            if (grille[i][jj].valPossib[p]==val && jj!=j && jj!=(j+k1) && jj!=(j+k2))
            {
                grille[i][jj]=suprimElementCase(grille[i][jj],val);
                if (grille[i][jj].nbrDePos==1)
                {
                    grille[i][jj]=validationDeCase(grille[i][jj],grille[i][jj].valPossib[0]);
                    initialiseposibilite(grille);
                }
            }
        }
    }

}










