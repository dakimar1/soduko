#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "typeCase.h"
#include "fonctionResolution.h"
#include "resolution.h"


/*
Description des donnees : une Grille non r�solue
resoud la grille en utilison R1,R2,R3,R4 et la recherche exaustive
Description des resultats : une Grille non r�solue
*/
void resolutionGrille(Case grille[9][9])
{
    initialiseposibilite(grille);
    applicationR2(grille);
    applicationR3(grille);
    applicationR4(grille);
    recherchExaustive(grille,0);
}
