#ifndef H_fonctionGrille

#define H_fonctionGrille


void affichageGrille(Case grille[9][9]);
void lectureGrille(char* fSource,Case grille[9][9]);
void ecriturGrille(char* fSource,Case grille[9][9]);
bool absent_R4(Case grille[9][9],int i,int j,int val);
bool absentSurLigne_R4(Case grille[9][9],int i,int j,int val);
void supprimer_R4(Case grille[9][9],int i,int j,int val);
bool absentSurLigne (int k,Case grille[9][9], int i);
bool absentSurColonne (int k, Case grille[9][9], int j);
bool absentSurRegion(int k, Case grille[9][9], int i, int j);
bool absentSurLigne_R3(int k,Case grille[9][9], int i, int j);
bool absentSurColonne_R3(int k,Case grille[9][9], int i, int j);
bool absentSurRegion_R3(int k,Case grille[9][9], int i, int j);

#endif
