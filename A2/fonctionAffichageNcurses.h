#ifndef H_fonctionAffichageNcurses

#define H_fonctionAffichageNcurses

#define WIDTH 14
#define HEIGHT 28

void affichagerGille1(WINDOW *grille_win,Case grille[9][9]);
void faireChoix(WINDOW *menu_win,Case grille[9][9],TAB_Grille TabGrille);

#endif
