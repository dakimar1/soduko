/*
 *	Name: Resolution d'un sudoku par aplication des regles avec ncurses
 *
 *	Desc:
 * Implementation des 4 regles R1, R2, R3 et R4 .
 * Les grilles sont lues a partir de fichiers textes.
 * On applique la regle que on veux 
 *
 *	Author: Daki Marouane	(daki.marouane@gmail.com).
 *			Douelfakar Omar  (douelfakar.omar@gmail.com).
 *
 *	Date: 25.Avr.2012
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ncurses.h>

#include "typeCase.h"
#include "fonctionResolution.h"
#include "fonctionGrille.h"

#include "Sauvgardage.h"
#include "fonctionAffichageNcurses.h"

int main(int argc,char* argv[])
{


    if (argc==0)//on teste si le nombre d'argument est bon
    {
        perror("Usage : fichierSource ");
        printf("Appuyer sur une touche pour quitter\n");
        getchar();
        exit(1);
    }
    if (fopen(argv[1],"r")==NULL)//on teste si le fichier existe
    {
        perror("ERROR Fichier\n");
        printf("Appuyer sur une touche pour quitter\n");
        getchar();
        exit(2);
    }// allez au suivant Cor : n
    WINDOW *grille_win;
    TAB_Grille tabGrille;
    tabGrille=initialise_sauvegarde(argv[1]);
    initscr(); // on initialise ncurses
    clear();
    noecho();
    cbreak();
    start_color();
    init_pair(2, COLOR_WHITE, COLOR_BLUE);
    init_pair(3, COLOR_BLUE,COLOR_WHITE );
    bkgd(' ' | COLOR_PAIR(2));
    grille_win = newwin(WIDTH, HEIGHT, 2, 15);
    box(grille_win,ACS_VLINE,ACS_HLINE);
    keypad(grille_win,FALSE);
    wbkgd(grille_win,COLOR_PAIR(3));
    refresh();
    affichagerGille1(grille_win, tabGrille->grille);
    faireChoix(grille_win,tabGrille->grille,tabGrille);
    clrtoeol();
    refresh();
    endwin();
    return 0;
}



