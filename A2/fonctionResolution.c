#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "typeCase.h"
#include "fonctionGrille.h"
#include "fonctionResolution.h"





/*
Description des donnees : une grille
S'il n'y a qu'un candidat a une case, alors c'est cette valeur qu'il
faut placer dans la case.
Description des resultats : rien
*/
void applicationR2(Case grille[9][9])
{
    int i,j;
    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            if (grille[i][j].nbrDePos==1)
            {
                grille[i][j]=validationDeCase(grille[i][j],grille[i][j].valPossib[0]);
                initialiseposibilite(grille);
                return ;
            }
        }
    }
}


/*
Description des donnees : une grille
Un candidat n'apparaissant que pour une seule case d'un groupe
peut ^etre place dans la case correspondante.
Description des resultats : rein
*/
void applicationR3(Case grille[9][9])
{
    int i,j,k;
    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            for(k=0; k<grille[i][j].nbrDePos; k++)
            {
                if (absentSurRegion_R3(grille[i][j].valPossib[k],grille,i,j)
                    || absentSurLigne_R3(grille[i][j].valPossib[k],grille,i,j)
                    || absentSurColonne_R3(grille[i][j].valPossib[k],grille,i,j))
                {
                    grille[i][j]=validationDeCase(grille[i][j],grille[i][j].valPossib[k]);
                    initialiseposibilite(grille);
                    return ;
                }
            }
        }
    }

}


/*
Description des donnees : une grille
dans une region donnee, lorsqu'un candidat n'est present
que dans des cases d'une seule ligne, on peut supprimer ce candidat des autres cases de
cette ligne (c'est a dire des cases de la meme ligne appartenant aux 2 autres regions)
Description des resultats : rien
*/
void applicationR4(Case grille[9][9])
{
    int i,j,k;
    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            for(k=0; k<grille[i][j].nbrDePos; k++)
            {
                if (absentSurLigne_R4(grille,i,j,grille[i][j].valPossib[k])
                        && absent_R4(grille,i,j,grille[i][j].valPossib[k])
                   )
                {

                    if (supprimer_R4(grille,i,j,grille[i][j].valPossib[k])) return;

                }
            }
        }
    }
}


/*
Description des donnees : une grille
on essaie de maniere systematique toutes les
grilles encore candidates jusqu'a trouver la bonne
Description des resultats : rien
*/
bool recherchExaustive(Case grille[9][9], int position)
{
    if (position == 9*9)
        return true;

    int i = position/9;
    int j = position%9;
    int k;
    if (grille[i][j].valide)
        return recherchExaustive(grille, position+1);

    for (k=0; k <9; k++)
    {
        if (absentSurLigne(k,grille,i) && absentSurColonne(k,grille,j) && absentSurRegion(k,grille,i,j))
        {
            grille[i][j] = validationDeCase(grille[i][j],k);

            if ( recherchExaustive (grille, position+1) )
                return true;
        }
    }
    grille[i][j]=deValidationDeCase(grille[i][j]);
    return false;
}


