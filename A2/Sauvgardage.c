#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "typeCase.h"
#include "fonctionGrille.h"
#include "Sauvgardage.h"



/*
Description des donnees : une chaine de caractere fSource
inisialise l'element TAB_grille avec le fichier fsource
Description des resultats : TAB_grille
*/
TAB_Grille initialise_sauvegarde(char* fSource)
{
    TAB_Grille t;
    t=(TAB_Grille)malloc(sizeof(struct T_GRILLE));
    lectureGrille(fSource,t->grille);
    t->suiv=NULL;
    t->prec=NULL;
    return t;
}

/*
Description des donnees : deux grille S_grille et D_grille
copie la grille S_grille dans D_grille
Description des resultats :
*/
void copiGrille(Case S_grille[9][9],Case D_grille[9][9])
{
    int i,j;
    for(i=0; i<9; i++)
    {
        for(j=0; j<9; j++)
        {
            D_grille[i][j].Valvalide=S_grille[i][j].Valvalide;
            D_grille[i][j].valide=S_grille[i][j].valide;
            D_grille[i][j].nbrDePos=0;
        }
    }
}

/*
Description des donnees : un element TAB_Grille G et une grille
ajoute la grille a l'element TAB_Grille
Description des resultats : TAB_Grille
*/
TAB_Grille ajoute_sauvegarde(TAB_Grille G,Case grille[9][9])
{
    TAB_Grille t;
    t=(TAB_Grille)malloc(sizeof(struct T_GRILLE));
    copiGrille(grille,t->grille);
    G->suiv=t;
    t->suiv=NULL;
    t->prec=G;
    return t;

}
/*
Description des donnees : un element TAB_Grille G
un regarde si il y a possibilite d'allez a la grille precedent
Description des resultats : vrai sinon faux
*/
bool possible_Go_prec(TAB_Grille G)
{
    return G->prec!=NULL;
}

/*
Description des donnees : un element TAB_Grille G
va a la grille precedent
Description des resultats : TAB_Grille
*/
TAB_Grille go_prec(TAB_Grille G)
{
    if (possible_Go_prec(G)){
        return G->prec;
    }else
        return G;
}

/*
Description des donnees : un element TAB_Grille G
un regarde si il y a possibilite d'allez a la grille suivant
Description des resultats : vrai sinon faux
*/
bool possible_Go_Suiv(TAB_Grille G)
{
    return G->suiv!=NULL;
}

/*
Description des donnees : un element TAB_Grille G
va a la grille suivant
Description des resultats : TAB_Grille
*/
TAB_Grille go_Suiv(TAB_Grille G)
{
    if (possible_Go_Suiv(G))
        return G->suiv;
    else
        return G;
}



