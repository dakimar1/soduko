#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ncurses.h>

#include "typeCase.h"
#include "fonctionResolution.h"
#include "fonctionGrille.h"

#include "Sauvgardage.h"


/*
Description des donnees : une fenetre grille_win, une grille
elle affiche les different regle de jeu mais aussi d'afficher la grille
Description des resultats :
*/
void affichagerGille1(WINDOW *grille_win,Case grille[9][9])
{
    int i,j,x,y;
    x = 2;
    y = 1;
    for(i=0; i<9; i++)
    {
        for(j=0,x=2; j<9; j++,x=x+2)
        {
            mvwprintw(grille_win, y, x, "%d", grille[i][j]);
            if ((j+1)%3==0 && j!=8)
            {
                x=x+2;
                mvwprintw(grille_win, y, x, "||");
                x++;
            }
        }
        y=y+1;
        if ((i+1)%3==0)
        {
            mvwprintw(grille_win, y, 2,"------------------------");
            y=y+1;
        }
    }

    wrefresh(grille_win);
    mvprintw(LINES-8, 0,"Echap  : pour Quitter");
    mvprintw(LINES-7, 0,"1      : appliquer R1");
    mvprintw(LINES-6, 0,"2      : appliquer R2");
    mvprintw(LINES-5, 0,"3      : appliquer R3");
    mvprintw(LINES-4, 0,"4      : appliquer R4");
    mvprintw(LINES-3, 0,"e      : appliquer Recherche Exaustive");
    mvprintw(LINES-2, 0,"n      : pour Next");
    mvprintw(LINES-1, 0,"b      : pour Back ");
    refresh();
}

/*
Description des donnees : une fenetre grille_win, une grille et un element TAB_Grille
elle nous permet d'utilis� les different regle de jeu mais aussi d'allez a la grille
precedent ou suivant
Description des resultats :
*/
void faireChoix(WINDOW *menu_win,Case grille[9][9],TAB_Grille TabGrille)
{
    int c;
    while(1)
    {
        c = wgetch(menu_win);
        switch(c)
        {
        case 49:// aplique R1 Cor : 1
            mvprintw(0, 0,"l'application R1 a etait applique");
            initialiseposibilite(grille);
            TabGrille=ajoute_sauvegarde(TabGrille,grille);
            break;
        case 50:// aplique R2 Cor : 2
            mvprintw(0, 0,"l'application R2 a etait applique");
            applicationR2(grille);
            TabGrille=ajoute_sauvegarde(TabGrille,grille);
            break;
        case 51:// aplique R3 Cor : 3
            mvprintw(0, 0,"l'application R3 a etait applique");
            applicationR3(grille);
            TabGrille=ajoute_sauvegarde(TabGrille,grille);
            break;
        case 52 :// aplique R4 Cor : 4
            mvprintw(0, 0,"l'application R4 a etait applique");
            applicationR4(grille);
            TabGrille=ajoute_sauvegarde(TabGrille,grille);
            break;
        case 101:  // recherche exaustive Cor : e
            mvprintw(0, 0,"la Recherche Exaustive a etait applique");
            recherchExaustive(grille,0);
            TabGrille=ajoute_sauvegarde(TabGrille,grille);
            break;
        case 98: // revenir precedament Cor : b
            mvprintw(0, 0,"allez au precedent");
            TabGrille=go_prec(TabGrille);
            copiGrille(TabGrille->grille,grille);
            break;
        case 110: // allez au suivant Cor : n
            mvprintw(0, 0,"allez au suivant");
            TabGrille=go_Suiv(TabGrille);
            copiGrille(TabGrille->grille,grille);
            break;
        case 27: // quitter Cor : echap
            return ;
            break;
        default:
            break;
        }
        affichagerGille1(menu_win,grille);
        refresh();
    }
}
