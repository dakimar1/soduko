#ifndef H_Sauvgardage

#define H_Sauvgardage

typedef struct T_GRILLE
{
    Case grille[9][9];
    struct T_GRILLE* suiv;
    struct T_GRILLE* prec;
} ;

typedef struct T_GRILLE* TAB_Grille;



TAB_Grille initialise_sauvegarde(char* fSource);
void copiGrille(Case S_grille[9][9],Case D_grille[9][9]);
TAB_Grille ajoute_sauvegarde(TAB_Grille G,Case grille[9][9]);
bool possible_Go_Suiv(TAB_Grille G);
bool possible_Go_prec(TAB_Grille G);
TAB_Grille go_Suiv(TAB_Grille G);
TAB_Grille go_prec(TAB_Grille G);

#endif




